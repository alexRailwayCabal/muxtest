package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func callHello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("The call seems to have worked")
}

func main() {
	fmt.Println("Hello")
	r := mux.NewRouter()
	r.HandleFunc("/Call", callHello)
	log.Fatal(http.ListenAndServe(":8080", r))
}
